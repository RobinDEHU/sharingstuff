function [e] = test2(cleanup, level)
%TEST2 pslaunch, psquit

%   Thomas Ruark, 11/01/2006
%   Copyright 2006 Adobe Systems, Inc.

if strcmp(computer, 'MAC') || strcmp(computer, 'MACI')
    extraErrorMsg = 'known issue doing pslaunch';
else
    extraErrorMsg = '';
    pslaunch();
end

if strcmp(computer, 'MAC') || strcmp(computer, 'MACI')
    extraErrorMsg = 'known issue doing pslaunch';
else
    extraErrorMsg = '';
    psquit();
end

e = ['PASS ' extraErrorMsg];

