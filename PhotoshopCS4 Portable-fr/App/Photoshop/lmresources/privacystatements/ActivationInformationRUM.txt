Activarea produsului Adobe

Activarea produsului este o măsură tehnică împotriva copierii neautorizate a software-ului Adobe.  Activarea este obligatorie.  Dacă nu activaţi acest produs, în cele din urmă nu îl veţi mai putea utiliza.  Pentru activarea acestui produs este posibil să fie necesar să apelaţi telefonic serviciul cu clienţii Adobe.

În timpul procesului de activare nu sunt furnizate nici un fel de informaţii personale companiei Adobe sau unui alt terţ.

La momentul instalării, acest produs şi calculatorul dvs. au creat un număr de activare care, împreună cu numărul de serie al produsului, va fi furnizat companiei Adobe pe parcursul procesului de activare.  Numărul de activare şi numărul de serie al produsului nu conţin informaţii personale şi nici informaţii despre producătorul sau modelul calculatorului dvs.  Dacă activaţi acest produs utilizând o conexiune la Internet, variabilele protocolului Internet necesare pentru stabilirea unei conexiuni prin Internet vor fi transmise companiei Adobe pe parcursul procesului de activare.  Aceste variabile pentru protocolul Internet includ tipul de browser utilizat şi adresa protocolului Internet.  Adobe nu corelează aceste variabile cu informaţiile personale.

Acest produs acceptă modificări ale configuraţiei calculatorului dvs.  Reconfigurările minore ale hardware-ului efectuate după activare nu vor necesita o activare suplimentară. Dacă înlocuiţi sau efectuaţi un upgrade al unităţii hard, sau dacă eliminaţi acest produs de pe un calculator cu licenţă şi îl instalaţi pe un alt calculator cu licenţă, este posibil să fie necesară reactivarea produsului.  

Activarea produsului este distinctă şi separată de înregistrarea acestuia.  Înregistrarea produsului este un proces voluntar pe parcursul căruia furnizaţi companiei Adobe anumite informaţii personale şi non-personale, cum ar fi numele dvs., adresa poştală, adresa de e-mail şi numărul de serie al produsului.  Toate informaţiile furnizate companiei Adobe pe parcursul procesului de înregistrare sunt stocate separat de informaţiile furnizate companiei Adobe pe parcursul activării.

Pentru mai multe informaţii privind activarea produsului, vizitaţi adresa http://www.adobe.com/activation

