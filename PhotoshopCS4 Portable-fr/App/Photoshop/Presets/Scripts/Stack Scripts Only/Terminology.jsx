// (c) Copyright 2005-2007  Adobe Systems, Incorporated.  All rights reserved.

/*
@@@BUILDINFO@@@ Terminology.jsx 1.0.0.0
*/

// This defines the Terminology (TypeID constants) used by StackSupport.jsx
// This is essentially an abridged translation of PITerminology.h

// Mode constants
const classBitmapMode			= app.charIDToTypeID('BtmM');
const classGrayscaleMode		= app.charIDToTypeID('Grys');
const classDuotoneMode			= app.charIDToTypeID('DtnM');
const classIndexedColorMode		= app.charIDToTypeID('IndC');
const classRGBColorMode			= app.charIDToTypeID('RGBM');
const classCMYKColorMode		= app.charIDToTypeID('CMYM');
const classLabColorMode			= app.charIDToTypeID('LbCM');
const classMultichannelMode		= app.charIDToTypeID('MltC');

const classAdjustmentLayer		= app.charIDToTypeID('AdjL');
const classApplication			= app.charIDToTypeID('capp');
const classBrightnessContrast	= app.charIDToTypeID('BrgC');
const classChannel				= app.charIDToTypeID('Chnl');
const classColor				= app.charIDToTypeID('Clr ');
const classColorBalance			= app.charIDToTypeID('ClrB');
const classColorStop			= app.charIDToTypeID('Clrt');
const classCurves				= app.charIDToTypeID('Crvs');
const classDocument				= app.charIDToTypeID('Dcmn');
const classFileSavePrefs		= app.charIDToTypeID('FlSv');
const classGradient				= app.charIDToTypeID('Grdn');
const classGrayscale			= app.charIDToTypeID('Grsc');
const classHueSaturation		= app.charIDToTypeID('HStr');
const classLayer				= app.charIDToTypeID('Lyr ');
const classLevels				= app.charIDToTypeID('Lvls');
const classMask					= app.charIDToTypeID('Msk ');
const classMenuItem				= app.charIDToTypeID('Mn  ');
const classPoint				= app.charIDToTypeID('Pnt ');
const classPolygon				= app.charIDToTypeID('Plgn');
const classProperty				= app.charIDToTypeID('Prpr');
const classRectangle			= app.charIDToTypeID('Rctn');
const classVersion				= app.charIDToTypeID('Vrsn');


const enumBackgroundColor	= app.charIDToTypeID('BckC');
const enumBlue				= app.charIDToTypeID('Bl  ');
const enumCustomStops		= app.charIDToTypeID('CstS');
const enumDarken			= app.charIDToTypeID('Drkn');
const enumFirst				= app.charIDToTypeID('Frst');
const enumFitOnScreen		= app.charIDToTypeID('FtOn');
const enumForegroundColor	= app.charIDToTypeID('FrgC');
const enumGray				= app.charIDToTypeID('Gry ');
const enumGreen				= app.charIDToTypeID('Grn ');
const enumHistory			= app.charIDToTypeID('Hsty');
const enumHorizontal		= app.charIDToTypeID('Hrzn');
const enumLinear			= app.charIDToTypeID('Lnr ');
const enumMask				= app.charIDToTypeID('Msk '	);// typeChannel.
const enumNone				= app.charIDToTypeID('None');
const enumRed 				= app.charIDToTypeID('Rd  ');
const enumRevealAll			= app.charIDToTypeID('RvlA');
const enumRGBColor			= app.charIDToTypeID('RGBC');
const enumTarget			= app.charIDToTypeID('Trgt');
const enumUserStop			= app.charIDToTypeID('UsrS');

const eventConvertMode		= app.charIDToTypeID('CnvM');
const eventDelete			= app.charIDToTypeID('Dlt ');
const eventDuplicate		= app.charIDToTypeID('Dplc');
const eventGet				= app.charIDToTypeID('getd');
const eventGradient			= app.charIDToTypeID('Grdn');
const eventImageSize		= app.charIDToTypeID('ImgS');
const eventMake				= app.charIDToTypeID('Mk  ');
const eventMove				= app.charIDToTypeID('move');
const eventPurge			= app.charIDToTypeID('Prge');
const eventSelect			= app.charIDToTypeID('slct');
const eventSet				= app.charIDToTypeID('setd');
const eventTransform		= app.charIDToTypeID('Trnf');
const eventUndo				= app.charIDToTypeID('undo');


// Stuff from PIStringTerminology.h
const kADSContentStr				= app.stringIDToTypeID("ADSContent");
const kaddLayerFromViewlessDocStr	= app.stringIDToTypeID("addLayerFromViewlessDoc");
const kaddToSelectionContinuousStr	= app.stringIDToTypeID("addToSelectionContinuous");
const kaskMismatchOpeningStr		= app.stringIDToTypeID("askMismatchOpening");
const kaskMismatchPastingStr		= app.stringIDToTypeID("askMismatchPasting");
const kaskMissingStr				= app.stringIDToTypeID("askMissing");
const kaspectRatioStr			= app.stringIDToTypeID("aspectRatio");
const kcameraRawJPEGStr			= app.stringIDToTypeID("cameraRawJPEG");
const kcameraRotationStr		= app.stringIDToTypeID("cameraRotation");
const kcloseViewlessDocumentStr = app.stringIDToTypeID("closeViewlessDocument");
const kcolorSettingsStr			= app.stringIDToTypeID("colorSettings");
const kcylindricalStr			= app.stringIDToTypeID("cylindrical");
const kdocumentStr				= app.stringIDToTypeID("document");
const kflatnessStr 				= app.stringIDToTypeID("flatness");
const kfocalLengthStr			= app.stringIDToTypeID("focalLength");
const kgeometryOnlyStr			= app.stringIDToTypeID("geometryOnly");
const kgeometryRecordStr		= app.stringIDToTypeID("geometryRecord");
const kgroupStr					= app.stringIDToTypeID("group");
const khighQualityStr			= app.stringIDToTypeID("highQuality");
const kignoreStr				= app.stringIDToTypeID("ignore");
const kimageCenterStr			= app.stringIDToTypeID("imageCenter");
const kinteractiveStr			= app.stringIDToTypeID("interactive");
const klayerIDStr				= app.stringIDToTypeID("layerID");
const klayersStr 				= app.stringIDToTypeID("layers");
const klayerTransformationStr	= app.stringIDToTypeID("layerTransformation");
const klensCorrectionStr		= app.stringIDToTypeID("lensCorrection");
const klightroomBridgetalkIDStr	= app.stringIDToTypeID("lightroomBridgetalkID");
const klightroomDocIDStr		= app.stringIDToTypeID("lightroomDocID");
const klightroomSaveParamsStr	= app.stringIDToTypeID("lightroomSaveParams");
const kmergeAlignedLayersStr	= app.stringIDToTypeID("mergeAlignedLayers");
const knewPlacedLayerStr		= app.stringIDToTypeID("newPlacedLayer");
const kopenViewlessDocumentStr	= app.stringIDToTypeID("openViewlessDocument");
const kpixelStr 				= app.stringIDToTypeID("pixel");
const kpreferXMPFromACRStr		= app.stringIDToTypeID("preferXMPFromACR");
const kprojectionStr			= app.stringIDToTypeID("projection");
const kquadCorner0Str			= app.stringIDToTypeID("quadCorner0");
const kquadCorner1Str			= app.stringIDToTypeID("quadCorner1");
const kquadCorner2Str			= app.stringIDToTypeID("quadCorner2");
const kquadCorner3Str			= app.stringIDToTypeID("quadCorner3");
const kquadrilateralStr			= app.stringIDToTypeID("quadrilateral");
const kradialDistortStr			= app.stringIDToTypeID("radialDistort");
const krectangleStr				= app.stringIDToTypeID("rectangle");
const kresetDocumentFormatStr	= app.stringIDToTypeID("resetDocumentFormatStr");
const ksceneCollageStr			= app.stringIDToTypeID("sceneCollage");
const kselectionModifierStr		= app.stringIDToTypeID("selectionModifier");
const kselectionModifierTypeStr	= app.stringIDToTypeID("selectionModifierType");
const ksphericalStr				= app.stringIDToTypeID("spherical");
const ktransformStr				= app.stringIDToTypeID("transform");
const ktranslationStr			= app.stringIDToTypeID("translation");
const kvignetteStr				= app.stringIDToTypeID("vignette");
const kXMPMetadataAsUTF8Str		= app.stringIDToTypeID("XMPMetadataAsUTF8");

const keyAddLayerFromFile	= app.stringIDToTypeID( "addLayerFromFile" );
const keyAlignment			= app.charIDToTypeID('Algn');
const keyAntiAlias			= app.charIDToTypeID('AntA');
const keyApply				= app.charIDToTypeID('Aply');
const keyAt					= app.charIDToTypeID('At  ');
const keyAuto				= app.charIDToTypeID('Auto');
const keyBottom				= app.charIDToTypeID('Btom');
const keyChannelName		= app.charIDToTypeID('ChnN'	);// Property. Breaks hash.
const keyDepth				= app.charIDToTypeID('Dpth');
const keyDither				= app.charIDToTypeID('Dthr');
const keyExposure			= app.charIDToTypeID('Exps');
const keyFileList		    = app.stringIDToTypeID( "fileList" );
const keyFileSavePrefs		= app.charIDToTypeID('FlSP');
const keyFrom				= app.charIDToTypeID('From');
const keyGamma				= app.charIDToTypeID('Gmm ');
const keyGradient			= app.charIDToTypeID('Grad');
const keyHeight				= app.charIDToTypeID('Hght');
const keyHorizontal			= app.charIDToTypeID('Hrzn');
const keyInterpolation		= app.charIDToTypeID('Intr');
const keyLeft				= app.charIDToTypeID('Left');
const keyLocation			= app.charIDToTypeID('Lctn');
const keyNumberOfChannels	= app.charIDToTypeID('NmbO');
const keyMakeVisible		= app.charIDToTypeID('MkVs');
const keyMethod				= app.charIDToTypeID('Mthd');
const keyMidpoint			= app.charIDToTypeID('Mdpn');
const keyMode				= app.charIDToTypeID('Md  ');
const keyName				= app.charIDToTypeID('Nm  ');
const keyNew				= app.charIDToTypeID('Nw  ');
const keyNull				= app.charIDToTypeID('null');
const keyOffset				= app.charIDToTypeID('Ofst');
const keyOpacity			= app.charIDToTypeID('Opct');
const keyPerspectiveIndex	= app.charIDToTypeID('Prsp');
const keyPoints				= app.charIDToTypeID('Pts ');
const keyResolution			= app.charIDToTypeID('Rslt');
const keyReverse			= app.charIDToTypeID('Rvrs');
const keyRight				= app.charIDToTypeID('Rght');
const keySelection			= app.charIDToTypeID('fsel');	// Defined in piActions.h
const keyTarget				= app.charIDToTypeID('null');;
const keyTo					= app.charIDToTypeID('T   ');
const keyTop				= app.charIDToTypeID('Top ');
const keyTransferSpec		= app.charIDToTypeID('TrnS');
const keyTransparency		= app.charIDToTypeID('Trns');
const keyType				= app.charIDToTypeID('Type');
const keyUseMask			= app.charIDToTypeID('UsMs');
const keyUserMaskEnabled	= app.charIDToTypeID('UsrM');
const keyUsing				= app.charIDToTypeID('Usng');
const keyVertical			= app.charIDToTypeID('Vrtc');
const keyViewlessDoc		= app.stringIDToTypeID( "viewlessDoc" );
const keyWidth				= app.charIDToTypeID('Wdth');

const typeAlignDistributeSelector	= app.charIDToTypeID('ADSt');
const typeBlendMode			= app.charIDToTypeID('BlnM');
const typeChannel			= app.charIDToTypeID('Chnl');
const typeColors			= app.charIDToTypeID('Clrs');
const typeColorStopType		= app.charIDToTypeID('Clry');
const typeGradientForm		= app.charIDToTypeID('GrdF');
const typeGradientType		= app.charIDToTypeID('GrdT');
const typeMenuItem			= app.charIDToTypeID('MnIt');
const typeNULL 				= app.charIDToTypeID('null');
const typeOrdinal			= app.charIDToTypeID('Ordn');
const typePurgeItem			= app.charIDToTypeID('PrgI');

const unitDensity			= app.charIDToTypeID('#Rsl');
const unitPercent			= app.charIDToTypeID('#Prc');
const unitPixels			= app.charIDToTypeID('#Pxl');
